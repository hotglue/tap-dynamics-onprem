"""REST client handling, including DynamicsOnpremStream base class."""

import requests
from typing import Any, Dict, Optional, cast

from singer_sdk.streams import RESTStream
from singer_sdk.authenticators import BasicAuthenticator
from pendulum import parse

class DynamicsOnpremStream(RESTStream):
    """DynamicsOnprem stream class."""

    @property
    def url_base(self) -> str:
        return self.config.get("base_url")
    
    records_jsonpath = "$.value[*]"
    items_per_call = 100

    @property
    def authenticator(self) -> BasicAuthenticator:
        """Return a new authenticator object."""
        return BasicAuthenticator.create_for_stream(
            self,
            username=self.config.get("username"),
            password=self.config.get("password"),
        )

    @property
    def http_headers(self) -> dict:
        """Return the http headers needed."""
        headers = {}
        if "user_agent" in self.config:
            headers["User-Agent"] = self.config.get("user_agent")
        return headers

    def get_next_page_token(
        self, response: requests.Response, previous_token: Optional[Any]
    ) -> Optional[Any]:
        """Return a token for identifying next page or None if no more pages."""
        values = response.json().get("value", [])
        if values:
            previous_token = previous_token or 0
            next_page_token = previous_token + self.items_per_call
            return next_page_token
        return None
    
    def get_starting_time(self, context):
        start_date = self.config.get("start_date")
        if start_date:
            start_date = parse(self.config.get("start_date"))
        rep_key = self.get_starting_timestamp(context)
        return rep_key or start_date

    def get_url_params(
        self, context: Optional[dict], next_page_token: Optional[Any]
    ) -> Dict[str, Any]:
        """Return a dictionary of values to be used in URL parameterization."""
        params: dict = {}
        params["$format"] = "json"
        params["$top"] = self.items_per_call
        if next_page_token:
            params["$skip"] = next_page_token
        start_date = self.get_starting_time(context)
        if self.replication_key and start_date:
            start_date = start_date.strftime("%Y-%m-%dT%H:%M:%S.%fZ")
            params["$filter"] = f"{self.replication_key} gt {start_date}"
        return params
