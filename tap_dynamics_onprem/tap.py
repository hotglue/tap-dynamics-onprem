"""DynamicsOnprem tap class."""

from typing import List

from singer_sdk import Tap, Stream
from singer_sdk import typing as th  
from tap_dynamics_onprem.streams import (
    CompaniesStream,
    AccountsStream,
    VendorsStream,
    ItemsStream
)

STREAM_TYPES = [
    CompaniesStream,
    AccountsStream,
    VendorsStream,
    ItemsStream
]


class TapDynamicsOnprem(Tap):
    """DynamicsOnprem tap class."""
    name = "tap-dynamics-onprem"

    config_jsonschema = th.PropertiesList(
        th.Property(
            "username",
            th.StringType,
            required=True,
        ),
        th.Property(
            "password",
            th.StringType,
            required=True,
        ),
        th.Property(
            "start_date",
            th.DateTimeType,
            description="The earliest record date to sync"
        ),
        th.Property(
            "base_url",
            th.StringType,
            required=True
        ),
    ).to_dict()

    def discover_streams(self) -> List[Stream]:
        """Return a list of discovered streams."""
        return [stream_class(tap=self) for stream_class in STREAM_TYPES]

if __name__ == "__main__":
    TapDynamicsOnprem.cli()
